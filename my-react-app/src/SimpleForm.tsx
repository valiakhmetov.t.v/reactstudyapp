import axios from 'axios';
import React, { Component } from 'react'; // let's also import Component
import './SimpleForm.scss'

type RowData = {
  id: number,
  title: string
  details: string
}

interface TableState {
  rows: RowData[];
  newId: number;
  newtitle: string;
  newdetails: string;
}

export const Row = ({id, title, details}: RowData) => (
  <tr key={id.toString()}>
    <td>{id}</td>
    <td>{title}</td>
    <td>{details}</td>
  </tr>
);

export class SimpleForm extends  Component<{}, TableState> {
  constructor(p: {}) {
    super(p);
    this.state = {
       rows: [],
       newId: 0,
       newtitle: '',
       newdetails: ''
    };

}
  
    
  render() {
    return (
      <div>
      <table className="table-latitude">
        <thead>
        <tr>
          <th>ID</th>
          <th>Title</th>
          <th>Details</th>
          </tr>
        </thead>
        <tbody>
        {this.state.rows.map((row) => {
            return <Row {...row} key={row.id}/>;
        })}
        </tbody>
      </table>
      <button onClick = {()=>this.GetRowData()}>Get some rows</button>
      <form onSubmit={this.handlePost} >
        <label>
          <input type="number" name="id" onChange={e=>this.setState({newId : parseInt(e.target.value)})}/>
        </label>
        <label>
          <input type="text" name="title" onChange={e=>this.setState({newtitle :e.target.value})}/>
        </label>
        <label>
          <input type="text" name="details" onChange={e=>this.setState({newdetails :e.target.value})}/>
        </label>
        <input type="submit" value="Send" />
      </form>
      </div>
    );
  }


  handlePost = (e: React.FormEvent) =>
  {
    e.preventDefault();
    axios.post(process.env.API_URL+'/RowData', {id:this.state.newId ,title:this.state.newtitle, details:this.state.newdetails});
  }

  GetRowData()
  {
    axios.get(process.env.API_URL+'/RowData').then(response=>response.data).then(json =>
      {
        this.setState({rows:json});
      }
    );
  }
}